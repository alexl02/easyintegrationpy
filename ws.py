from socketIO_client_nexus import SocketIO
import sys
import os
from requests.exceptions import ConnectionError
import threading
from generarPdv import *
from log import *
import logging

#logging.getLogger('socketIO-client').setLevel(logging.DEBUG)
#logging.basicConfig()

def on_connect(data):
    print('connect')

def on_disconnect():
    print('disconnect')

def on_reconnect():
    print('reconnect')

def on_messages(*args):
    #print(args)
    socketIO.emit('response', {'response': 'I receive a message'})

def on_confirm(*args):
    print("")

def generarPlano(invoice):
    generarArchivoPlano(invoice, socketIO)

def on_invoice(*args):
    #print(args)
    t = threading.Thread(target=generarPlano, args=(args[0],))
    threads.append(t)
    t.start()    

def on_identify(*args):
    #print(args)
    socketIO.emit('identify', {'companyId': '5f47f7f0c8ae1e183e5dcb25'})
    
threads = list()
socketIO = SocketIO('localhost', 8080)
"""socketIO.on('connect', on_connect)  
socketIO.on('disconnected', on_disconnect)
socketIO.on('reconnected', on_reconnect)"""
socketIO.on('identify', on_identify)
socketIO.on('invoice', on_invoice)
guardarLog("Agente iniciado.")
#socketIO.on('messages', on_messages)
#socketIO.on('confirm', on_confirm)

socketIO.wait()
#socketIO.wait(seconds=1)

