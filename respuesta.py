def getResponse(res, ok):
    #print res
    r2 = {}
    msg = ""
    r = 0
    if res.find(ok) > -1:
        r = 1
        msg = "OK"
    elif res.find(b">>>") > -1:
        #print "ERROR!"
        if res.find(b"1195") > -1:
            r = 2
            msg = "ERROR: EXISTEN CAJAS SIN CERRAR"
        elif res.find(b"1196") > -1:
            r = 2
            msg = "ERROR: NO HAY CAJAS PARA PROCESAR"
        elif res.find(b"0098") > -1:
            r = 2
            msg = "ERROR: 98 EN UN ARCHIVO, POR FAVOR REVISAR"
        elif res.find(b"0099") > -1:
            r = 2
            msg = "ERROR: 99 REGISTRO UTILIZADO POR OTRO USUARIO"
        elif res.find(b"0037") > -1:
            r = 2
            msg = "ERROR: 37 POR FAVOR REVISAR PERMISOS DEL DIRECTORIO EMPRESA"
        elif res.find(b"8061") > -1:
            r = 2
            msg = "ERROR: NO HAY ARCHIVO PARA PROCESAR"
        elif res.find(b"1385") > -1:
            r = 2
            msg = "NO SE ENCONTRO EL ARCHIVO DE TRANSMISION"
        elif res.find(b"2203") > -1:
            r = 2
            msg = "LA FECHA SE ENCUENTRA CERRADA"
        elif res.find(b"1002") > -1:
            r = 2
            msg = "USUARIO NO CATALOGADO"
        elif res.find(b"1004") > -1:
            r = 2
            msg = "ACCESO NEGADO AL SISTEMA"
    #elif res.find(">>>") > -1:
    #    r = 2
    #    msg = "ERROR: ERROR DESCONOCIDO"
    #elif res.find("[1]") > -1:
    #    r = 2
    #    msg = "ERROR: SE ENCONTRARON INCONSISTENCIAS POR FAVOR REVISAR"
    #elif res.find("Visualiza") > -1:
    #    r = 2
    #    msg = "ERROR: SE ENCONTRARON INCONSISTENCIAS POR FAVOR REVISAR"
    elif res.find(b"NO RECIBIDA") > -1:
        r = 2
        msg = "ERROR: SE ENCONTRARON INCONSISTENCIAS POR FAVOR REVISAR"
    #if r > 0:
    #    print "R: " + str(r) + " MSG: " + msg
    r2["r"] = r
    r2["msg"] = msg
    return r2

def getResponse2(res):
    r2 = {}
    r = 0
    msg = ""
    if res.find(b"Visualiza") > -1:
        r = 2
        msg = "ERROR: SE ENCONTRARON INCONSISTENCIAS POR FAVOR REVISAR"
    else:
        r = 1
        msg = "OK"
    #if r > 0:
    #    print "R: " + str(r) + " MSG: " + msg
    r2["r"] = r
    r2["msg"] = msg
    return r2
