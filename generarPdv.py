import sys
import os
import json
import traceback
import time
from datetime import datetime
from recepcion_venta import *
from log import *

def getVenta():
    try:
        archivo_venta = open("venta.json", "r")
        venta_json = archivo_venta.read()
        archivo_venta.close()
        venta = json.loads(venta_json)
        #print venta
        return venta
    except Exception as ex:
        print("Error al leer venta: " + str(ex))
        print(traceback.format_exc())

def generarArchivoPlano(invoice, socketIO):
    venta = invoice
    guardarLog("Generando archivo plano factura: " + str(venta["registerDocumentNumber"]) + "...")
    datos = ""
    #venta = getVenta()
    #print(venta)
    tipo_registro = "01"
    fecha_caja = formatearTexto(venta["registerDate"], 8, " ")
    co = venta["co"]
    numero_caja = venta["registerNumber"]
    numero_documento_caja = formatearNumero(str(venta["registerDocumentNumber"]), 6, "0")
    tipo_documento_factura = venta["invoiceDocumentType"]
    numero_factura_documento = formatearNumero(str(venta["invoiceDocumentNumber"]), 6, "0")
    items = venta["items"]
    for item in items:
        id_item = item["idItem"]
        cantidad = item["cantidad"]
        precio_unitario = int(item["precioUnitario"])
        porcentaje_descuento = int(item["porcentajeDescuento"])
        porcentaje_iva = item["porcentajeIVA"]
        unidad_medida = item["unidadMedida"]
        factor_unidad_medida = item["factorUnidadMedida"]
        indicador_item = "I"
        extencion_item = formatearTexto("", 3, " ")
        lista_precios = formatearTexto("", 3, " ")
        lista_descuento = formatearTexto("", 2, " ")
        comanda_servicio = formatearNumero("", 6, "0")
        motivo_venta = formatearTexto("", 2, " ")
        codigo_barras = formatearTexto("", 15, " ")
        referencia = formatearTexto("", 15, " ")

        valor_total = precio_unitario * cantidad
        liquidacion_descuento = int(valor_total * (porcentaje_descuento / 100))
        liquidacion_iva = int((valor_total * (1 + (porcentaje_descuento / 100))) * (porcentaje_iva / 100))
        valor_total = valor_total - liquidacion_iva
        #print(liquidacion_iva)
        valor_neto = int((valor_total - liquidacion_descuento) + liquidacion_iva)
        #valor_neto = int((valor_total - liquidacion_descuento))
        #print(valor_neto)

        s_cantidad = formatearNumero2(cantidad, 3, 13, "0", True)
        s_precio_unitario = formatearNumero2(precio_unitario, 2, 12, "0", True)
        s_valor_total = formatearNumero2(valor_total, 2, 14, "0", True)
        s_porcentaje_descuento = formatearPorcentaje(porcentaje_descuento, 2, 4, "0")
        s_liquidacion_descuento = formatearNumero2(liquidacion_descuento, 2, 12, "0", True)
        s_porcentaje_iva = formatearPorcentaje(porcentaje_iva, 2, 4, "0")
        s_liquidacion_iva = formatearNumero2(liquidacion_iva, 2, 12, "0", True)
        s_valor_neto = formatearNumero2(valor_neto, 2, 14, "0", True)
        s_factor_unidad_medida = formatearNumero2(factor_unidad_medida, 4, 9, "0", False)

        """print("Tipo de registro: " + tipo_registro + " " + str(len(tipo_registro)))
        print("Fecha caja: " + fecha_caja + " " + str(len(fecha_caja)))
        print("CO: " + co + " " + str(len(co)))
        print("Numero de caja: " + numero_caja + " " + str(len(numero_caja)))
        print("Numero de documento caja: " + numero_documento_caja + " " + str(len(numero_documento_caja)))
        print("Tipo documento factura: " + tipo_documento_factura + " " + str(len(tipo_documento_factura)))
        print("Numero de factura documento: " + numero_factura_documento + " " + str(len(numero_factura_documento)))
        print("Codigo de item: " + id_item + " " + str(len(id_item)))
        print("Extencion de item: " + extencion_item + " " + str(len(extencion_item)))
        print("Cantidad: " + s_cantidad + " " + str(len(s_cantidad)))
        print("Lista de precios: " + lista_precios + " " + str(len(lista_precios)))
        print("Lista de descuentos: " + lista_descuento + " " + str(len(lista_descuento)))
        print("Comanda de servicio: " + comanda_servicio + " " + str(len(comanda_servicio)))
        print("Precio unitario: " + s_precio_unitario + " " + str(len(s_precio_unitario)))
        print("Valor total: " + s_valor_total + " " + str(len(s_valor_total)))
        print("Porcentaje de descento: " + s_porcentaje_descuento + " " + str(len(s_porcentaje_descuento)))
        print("Liquidacion descuento: " + s_liquidacion_descuento + " " + str(len(s_liquidacion_descuento)))
        print("Porcentaje IVA: " + s_porcentaje_iva + " " + str(len(s_porcentaje_iva)))
        print("Liquidacion IVA: " + s_liquidacion_iva + " " + str(len(s_liquidacion_iva)))
        print("Valor neto: " + s_valor_neto + " " + str(len(s_valor_neto)))
        print("Movimiento venta: " + motivo_venta + " " + str(len(motivo_venta)))
        print("Unidad medida: " + unidad_medida + " " + str(len(unidad_medida)))
        print("Factor unidad medida: " + s_factor_unidad_medida + " " + str(len(s_factor_unidad_medida)))
        print("Indicador item: " + indicador_item + " " + str(len(indicador_item)))
        print("Codigo barras: " + codigo_barras + " " + str(len(codigo_barras)))
        print("Referencia: " + referencia + " " + str(len(referencia)))"""

        datos = datos + tipo_registro + fecha_caja + co + numero_caja + numero_documento_caja + tipo_documento_factura + numero_factura_documento + id_item + extencion_item + s_cantidad + lista_precios + lista_descuento + comanda_servicio + s_precio_unitario + s_valor_total + s_porcentaje_descuento + s_liquidacion_descuento + s_porcentaje_iva + s_liquidacion_iva + s_valor_neto + motivo_venta + unidad_medida + s_factor_unidad_medida + indicador_item + codigo_barras + referencia + "\n"

    tipo_registro = "02"
    tipo_recaudo = venta["paymentType"]
    codigo_medio_recaudo = formatearTexto(venta["paymentMethodCode"], 3, " ")
    indicador_ingreso_egreso = venta["ingressEgressIndicator"]
    valor_recaudo = venta["paymentValue"]
    codigo_entidad_financiera = formatearTexto("", 4, " ")
    numero_cheque = formatearNumero("", 6, "0")
    numero_cuenta_cheque = formatearTexto("", 20, " ")
    fecha_consignacion_cheque = formatearNumero("", 8, "0")
    referencia_medio_recaudo_otros = formatearTexto("", 10, " ")
    numero_cuenta_medio_recaudo_otros = formatearTexto("", 20, " ")
    valor_propina_medio_recaudo_otros = formatearNumero2(0, 2, 14, "0", True)
    valor_iva_medio_recaudo_otros = formatearNumero2(0, 2, 14, "0", True)
    codigo_cuenta_bancaria_medio_recaudo_consignacion = formatearTexto("", 3, " ")
    numero_consignacion = formatearNumero("", 6, "0")
    codigo_condicion_pago_medio_pago_cartera = formatearTexto("", 2, " ")
    codigo_cliente_factura_recaudo_cartera = formatearTexto("", 13, " ")
    codigo_sucursal_cliente_medio_recaudo_cartera = formatearTexto("", 2, " ")
    valor_recaudo_moneda_extranjera = formatearNumero2(0, 2, 18, "0", True)
    numero_autorizacion_medio_recaudo_cheque = formatearTexto("", 15, " ")
    observacion_medio_recaudo_cheque_consignacion_otros = formatearTexto("", 30, " ")

    s_valor_recaudo = formatearNumero2(valor_recaudo, 2, 18, "0", True)

    datos = datos + tipo_registro + fecha_caja + co + numero_caja + numero_documento_caja + tipo_recaudo + codigo_medio_recaudo + indicador_ingreso_egreso + codigo_entidad_financiera + numero_cheque + numero_cuenta_cheque + fecha_consignacion_cheque + referencia_medio_recaudo_otros + numero_cuenta_medio_recaudo_otros + valor_propina_medio_recaudo_otros + valor_iva_medio_recaudo_otros + codigo_cuenta_bancaria_medio_recaudo_consignacion + numero_consignacion + codigo_condicion_pago_medio_pago_cartera + codigo_cliente_factura_recaudo_cartera + codigo_sucursal_cliente_medio_recaudo_cartera + s_valor_recaudo + valor_recaudo_moneda_extranjera + numero_autorizacion_medio_recaudo_cheque + observacion_medio_recaudo_cheque_consignacion_otros

    #print datos
    r = guardar_archivo(datos, numero_documento_caja)
    print(numero_documento_caja + " " + r)
    if r.find("OK") > -1:
        guardarLog("Factura: " + numero_documento_caja + " generada correctamente." )
        socketIO.emit('invoiceReception', { 'status': "OK", 'invoiceNumber': numero_documento_caja })
    else:
        guardarLog("Error al generar la factura: " + numero_documento_caja + " " + r)
        socketIO.emit('invoiceReception', { 'status': "ERROR", 'invoiceNumber': numero_documento_caja })




def formatearTexto(texto, longitud, relleno):
    if len(texto) < longitud:
        while len(texto) < longitud:
            texto = texto + relleno
    return texto

def formatearNumero(numero, longitud, relleno):
    if len(numero) < longitud:
        prefijo = ""
        cantidad = longitud - len(numero)
        while len(prefijo) < cantidad:
            prefijo = prefijo + relleno
        numero = prefijo + numero
    return numero

def formatearNumero2(numero, decimales, longitud, relleno, mas):
    multiplo_decimales = "1"
    x = 0
    while x < decimales:
        multiplo_decimales = multiplo_decimales + "0" 
        x = x + 1

    s_numero = str(numero * int(multiplo_decimales))
    if mas:
        s_numero = s_numero + "+"

    if len(s_numero) < longitud:
        prefijo = ""
        cantidad = longitud - len(s_numero)
        while len(prefijo) < cantidad:
            prefijo = prefijo + relleno
        s_numero = prefijo + s_numero
    
    return s_numero    

def formatearPorcentaje(numero, decimales, longitud, relleno):
    multiplo_decimales = "1"
    x = 0
    while x < decimales:
        multiplo_decimales = multiplo_decimales + "0" 
        x = x + 1

    s_numero = str(numero * int(multiplo_decimales))

    if len(s_numero) < longitud:
        prefijo = ""
        cantidad = longitud - len(s_numero)
        while len(prefijo) < cantidad:
            prefijo = prefijo + relleno
        s_numero = prefijo + s_numero
    
    return s_numero    

def guardar_archivo(datos, numero_factura):
    formato = "%H%M%S"
    formato2 = "%f"
    hoy = datetime.today()
    ms = hoy.strftime(formato2)[0:2]
    dia = hoy.strftime(formato) + ms

    archivo = open("plain/" + dia + ".PO0", "w")
    archivo.write(datos)
    archivo.close()
    r = recepcionar_venta(dia, "0", numero_factura)
    return r