import os
import sys
import telnetlib
from respuesta import *

def recepcionar_venta(nombre_archivo, numero_archivo, numero_factura):
    archivo_log_screen = open("log/agent_screen_.log", "a")
    archivo_log_screen_factura = open("log/agent_screen_" + numero_factura + ".log", "a")
    os.system("scp plain/" + nombre_archivo + ".PO" + numero_archivo + " root@192.168.31.9:/pruebas/autjar/trm/")
    os.system('ssh root@192.168.31.9 "chmod -R 777 /pruebas/autjar/trm"')

    host = "192.168.31.9"
    user = "pruebas85"
    passwd = ""
    uuser = "ALEX02"
    password = "4RT3M1S4"
    if len(password) < 8:
        password = password + "\n"
    #print("INICIANDO LA RECEPCION DEL ARCHIVO DE VENTAS...\n")
    tn = telnetlib.Telnet(host)
    tn.set_debuglevel(0)

    if tn.expect([b"login: "]):
    #tn.read_until("login: ")
        tn.write(user.encode('utf8') + b"\n")

    if len(passwd) > 0:
        tn.read_until(b"Password: ")
        tn.write(passwd.encode('utf8') + b"\n")

    (i, obj, res) = tn.expect([b"9999999999"], 10)
    archivo_log_screen.write(res.decode('ascii'))
    archivo_log_screen_factura.write(res.decode('ascii'))

    if res.find(b"Usuario") > -1:
        if res.find(b"IMPORTANCIA") > -1:
            tn.write(b"\r")
    else:
        msg = "ERROR AL ENTRAR AL SISTEMA"
        return msg

    #tn.read_until("Usuario  :")
    tn.write(uuser.encode('utf8') + b"\n")
    #tn.read_until("Seguridad")
    tn.write(password.encode('utf8'))
    #time.sleep(2)
    tn.write(b"OAITV")
    tn.write(nombre_archivo.encode('utf8') + numero_archivo.encode('utf8'))
    #tn.write(b"\x1b[[B")
    tn.write(b"\r")
    tn.write(b"\r")
    tn.write(b"EW")
    tn.write(b"\r")
    tn.write(b"S")
    #print(tn.read_all())
    while True:
        (i, obj, res) = tn.expect([b"9999999999"], 1)
        archivo_log_screen.write(res.decode('unicode-escape'))
        archivo_log_screen_factura.write(res.decode('unicode-escape'))
        if res.find(b"PRECAUCION") > -1:
            tn.write(b"S")
        else:
            res = getResponse(res, b"FIN")
            r = res["r"]
            resmsg = res["msg"]
            if r > 0:
                break

    if r == 1:
        tn.write(b"\r")
        while True:
            (i, obj, res) = tn.expect([b"9999999999"], 1)
            archivo_log_screen.write(res.decode('unicode-escape'))
            archivo_log_screen_factura.write(res.decode('unicode-escape'))
            res = getResponse2(res)
            r = res["r"]
            resmsg = res["msg"]
            if r > 0:
                break
        if r == 1:
            msg = resmsg + " ARCHIVO DE VENTAS RECEPCIONADO CORRECTAMENTE"
        else:
            msg = "FASE 2: " + resmsg
    else:
        msg = resmsg
    #print(tn.read_all().decode('ascii'))
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    tn.write(b"\x1b")
    (i, obj, res) = tn.expect([b"9999999999"], 1)
    archivo_log_screen.write(res.decode('unicode-escape'))
    archivo_log_screen_factura.write(res.decode('unicode-escape'))
    tn.write(b"\x1b[21~")
    archivo_log_screen.close()
    archivo_log_screen_factura.close()
    tn.close()
    return msg
